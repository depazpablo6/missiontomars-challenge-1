package classes

import kotlin.random.Random

class U2 : Rocket (120.00, 18.00, 29.00) {

    override fun land(): Boolean {
        chanceCrash = 0.05 * ( weight / maxWeight)
        return Random.nextInt(101) > chanceCrash * 100
    }

    override fun launch(): Boolean {
        chanceExplode = 0.01 * ( weight / maxWeight )
        return Random.nextInt(101) > chanceExplode * 100
    }

}