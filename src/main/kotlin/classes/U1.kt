package classes

import kotlin.random.Random

class U1 : Rocket (100.00, 10.00, 18.00) {

    override fun land(): Boolean {
        chanceCrash = 0.05 * ( weight / maxWeight)
        return Random.nextInt(100) + 1 > chanceCrash * 100 //Testing the landing base on a random number
    }

    override fun launch(): Boolean {
        chanceExplode = 0.01 * ( weight / maxWeight )
        return Random.nextInt(100) + 1 > chanceExplode * 100 //Testing the launching base on a random number
    }

}