package classes

import interfaces.SpaceShip

open class Rocket (
    val cost : Double, //The cost of building the rocket
    var weight : Double, //The actual weight of the rocket
    val maxWeight: Double, //The maximum weight of the rocket
        ) : SpaceShip {

    var chanceExplode : Double = 0.00 //Probability of explode on lauching
    var chanceCrash : Double = 0.00 //Probability of crash while landing
    var shield : Boolean = false

    override fun launch(): Boolean {
        return true
    }

    override fun land(): Boolean {
        return true
    }

    override fun canCarry(item: Item): Boolean {
        //If the actual weight plus the item weight is greater than the max weight we cannot add it
        return weight + item.weight <= maxWeight
    }

    override fun carry(item: Item) {
        //Adding the item weight to the rocket weight
        weight += item.weight
    }
}