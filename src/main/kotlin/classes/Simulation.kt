package classes

import java.io.BufferedReader
import java.io.File
import java.io.InputStream
import java.io.InputStreamReader
import java.net.URL

class Simulation {

    var nCrashes : Int = 0
    var nExplosions: Int = 0
    var launchedRockets : Int = 0

    //Load Items via a TXT file on the web
    fun loadItems (fileUrl : String) : ArrayList<Item> {
        var items = ArrayList<Item>()
        var urlContent = URL (fileUrl)
        val reader = BufferedReader(InputStreamReader(urlContent.openStream()))//Reader of online file
        var actualLine : String?
        while (reader.readLine().also { actualLine = it } != null){
            //The actual line is divided by two, thanks to = symbol
            actualLine!!.split("=").let {
                //Assign the first part as the name and the second as the weight
                items.add(Item(it[0].trim(), it[1].trim().toDouble().div(1000.00))) //The weight is converted into Kgs
            }
        }
        reader.close()
        return items
    }

    //Its the same but, with the download file and read locally
    fun loadItemsLocally(fileUrl : String) : ArrayList<Item>{
        var items = ArrayList<Item>()
        val file : InputStream = File(fileUrl).inputStream()
        file.bufferedReader().forEachLine {
            it.split("=").let {
                items.add(Item(it[0].trim(), it[1].trim().toDouble().div(1000.00)))
            }
        }
        return items
    }

    //Returns a fleet of U1 Rockets
    fun loadU1 (items : ArrayList<Item>) : ArrayList<U1>{
        var u1Rockets = ArrayList<U1>()
        while(items.any()){//If there is still items this will create a new rocket
            var usedItems = ArrayList<Item>()
            var u1 = U1()
            for (i in items){
                if (u1.canCarry(i)) {//If the actual rocket can still carry this item
                    u1.carry(i)//it will carry it
                    usedItems.add(i)
                }
            }
            u1Rockets.add(u1)
            items.removeAll(usedItems)
        }

        return u1Rockets
    }

    //Returns a fleet of U1 Rockets
    fun loadU2 (items : ArrayList<Item>) : ArrayList<U2>{
        var u2Rockets = ArrayList<U2>()
        while(items.any()){//If there is still items this will create a new rocket
            var usedItems = ArrayList<Item>()
            var u2 = U2()
            for (i in items){
                if (u2.canCarry(i)) {//If the actual rocket can still carry this item
                    u2.carry(i)//it will carry it
                    usedItems.add(i)
                }
            }
            u2Rockets.add(u2)
            items.removeAll(usedItems)
        }

        return u2Rockets
    }

    //it returns the total budget on millions of the simulations
    fun <T : Rocket> runSimulation (rockets : ArrayList<T>) : Double {
        var budget : Double = 0.00
        nCrashes = 0
        nExplosions = 0
        launchedRockets = 0
        rockets.forEach{
            do {
                launchedRockets++//Number of tries or needed rockets to safely send it all
                budget += it.cost
                if(launchedRockets % 3 == 0) it.setShield() else it.removeShield() //Every 3 launched rockets the rocket has a shield
                var notExplode = it.launch()
                var notCrash = true
                if(notExplode) notCrash = it.land() //If the rocket didn't explode on launch we try to land it
                else {
                    nExplosions++
                    println("Rocket #${rockets.indexOf(it) + 1} BLOW UP")
                }
                if (!notCrash) {
                    nCrashes++
                    println("Rocket #${rockets.indexOf(it) + 1} CRASHED")
                }
                if (it.shield) println("Rocket #${rockets.indexOf(it) + 1} - Launched Rocket #${launchedRockets} with Shield Landed Succesfully")
            } while (!(notExplode && notCrash)) //If the rocket crash or explode we send it again
        }
        return budget
    }

    private fun Rocket.setShield () {
        shield = true
    }

    private fun Rocket.removeShield () {
        shield = false
    }

}
