import classes.Simulation

fun main(args: Array<String>){

    val urlContent1 = "https://s3.amazonaws.com/video.udacity-data.com/topher/2017/December/5a372d67_phase-1/phase-1.txt"
    val urlContent2 = "https://s3.amazonaws.com/video.udacity-data.com/topher/2017/December/5a372d88_phase-2/phase-2.txt"
    val urlLocal1 = "phase-1.txt"
    val urlLocal2 = "phase-2.txt"
    val simulation = Simulation()

    /*var phase1Item = simulation.loadItems(urlContent1)
    var phase2Item = simulation.loadItems(urlContent2)*/
    var phase1Item = simulation.loadItemsLocally(urlLocal1)
    var phase2Item = simulation.loadItemsLocally(urlLocal2)

    var u1P1Rockets = simulation.loadU1(phase1Item)
    var u1P2Rockets = simulation.loadU1(phase2Item)

    /*phase1Item = simulation.loadItems(urlContent1)
    phase2Item = simulation.loadItems(urlContent2)*/
    phase1Item = simulation.loadItemsLocally(urlLocal1)
    phase2Item = simulation.loadItemsLocally(urlLocal2)

    var u2P1Rockets = simulation.loadU2(phase1Item)
    var u2P2Rockets = simulation.loadU2(phase2Item)

//    (0 .. 100).forEach{
    println("BEGINNING OF SIMULATION")
    println("================================================\n")
    println("----- Simulation 1 - Rockets U1 - Phase 1 -----")
    println("N° of Rockets: ${u1P1Rockets.size}")
    println("_-_-_-_-_-_-_-_-_- Events _-_-_-_-_-_-_-_-_-_-_-_-")
    println("Total Budget: $${simulation.runSimulation(u1P1Rockets)} Millions")
    println("N° of used Rockets: ${simulation.launchedRockets}")
    println("N° of Explosions: ${simulation.nExplosions} - N° of Crashes: ${simulation.nCrashes}\n")
    println("----- Simulation 2 - Rockets U1 - Phase 2 -----")
    println("N° of Rockets: ${u1P2Rockets.size}")
    println("_-_-_-_-_-_-_-_-_- Events _-_-_-_-_-_-_-_-_-_-_-_-")
    println("Total Budget: $${simulation.runSimulation(u1P2Rockets)} Millions")
    println("N° of used Rockets: ${simulation.launchedRockets}")
    println("N° of Explosions: ${simulation.nExplosions} - N° of Crashes: ${simulation.nCrashes}\n")
    println("----- Simulation 3 - Rockets U2 - Phase 1 -----")
    println("N° of Rockets: ${u2P1Rockets.size}")
    println("_-_-_-_-_-_-_-_-_- Events _-_-_-_-_-_-_-_-_-_-_-_-")
    println("Total Budget: $${simulation.runSimulation(u2P1Rockets)} Millions")
    println("N° of used Rockets: ${simulation.launchedRockets}")
    println("N° of Explosions: ${simulation.nExplosions} - N° of Crashes: ${simulation.nCrashes}\n")
    println("----- Simulation 4 - Rockets U2 - Phase 2 -----")
    println("N° of Rockets: ${u2P2Rockets.size}")
    println("_-_-_-_-_-_-_-_-_- Events _-_-_-_-_-_-_-_-_-_-_-_-")
    println("Total Budget: $${simulation.runSimulation(u2P2Rockets)} Millions")
    println("N° of used Rockets: ${simulation.launchedRockets}")
    println("N° of Explosions: ${simulation.nExplosions} - N° of Crashes: ${simulation.nCrashes}\n")
    println("================================================")
    println("END OF SIMULATION")
//    }

}